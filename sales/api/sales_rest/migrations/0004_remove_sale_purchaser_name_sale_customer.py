# Generated by Django 4.0.3 on 2022-12-08 17:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_sale_delete_sale_record'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sale',
            name='purchaser_name',
        ),
        migrations.AddField(
            model_name='sale',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sale', to='sales_rest.customer'),
        ),
    ]
